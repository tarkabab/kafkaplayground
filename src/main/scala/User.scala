import java.io.ByteArrayOutputStream

import User.user_schema
import org.apache.avro.Schema
import org.apache.avro.generic.{GenericData, GenericRecord}
import org.apache.avro.io.EncoderFactory
import org.apache.avro.specific.SpecificDatumWriter

case class User(id: Int, name: String, email: String) {

  lazy val serialized: Array[Byte] = {
    val record = new GenericData.Record(user_schema)
    record.put("id", id)
    record.put("name", name)
    record.put("email", email)
    serialize(record)
  }

  private def serialize(record: GenericData.Record): Array[Byte] = {
    val writer = new SpecificDatumWriter[GenericRecord](user_schema)
    val out = new ByteArrayOutputStream()
    val encoder = EncoderFactory.get().binaryEncoder(out, null)
    writer.write(record, encoder)
    encoder.flush()
    out.close()
    out.toByteArray
  }

}

object User {

  val user_schema_descriptor: String = """
  | {
  |   "namespace": "kakfa-avro.test",
  |   "type": "record",
  |   "name": "user",
  |   "fields":[
  |     {  "name": "id", "type": "int"},
  |     {   "name": "name",  "type": "string"},
  |     {   "name": "email", "type": ["string", "null"]}
  |   ]
  | }
  """.stripMargin

  val user_schema: Schema = new Schema.Parser().parse(user_schema_descriptor)

}
