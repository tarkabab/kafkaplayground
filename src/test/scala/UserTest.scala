import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

//noinspection TypeAnnotation
class UserTest extends AnyWordSpec with Matchers {

  val user = User(1,"Peter","peter@somewhere.in")

  "User" should {

    "provide avro serialization" in {

      val user = User(1,"Peter","peter@somewhere.in")
      val expected = Array[Byte](2, 10, 80, 101, 116, 101, 114, 0, 36, 112, 101, 116, 101, 114, 64, 115, 111, 109, 101, 119, 104, 101, 114, 101, 46, 105, 110)

      user.serialized shouldBe expected
    }

  }
}
