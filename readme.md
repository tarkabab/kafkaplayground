### Setup

```
kubectl create -f zookeeper.yml
kubectl create -f kafka-service.yml
kubectl apply -f kafka-broker.yml
kubectl apply -f network.yml

kubectl describe svc kafka-service

kubectl exec -it kafka-broker0-64844d4856-6ds5z -- bash
kubectl delete pod kafka-broker0-64844d4856-6ds5z
```


### KafCat
https://github.com/edenhill/kafkacat
```
brew install kafkacat

kafkacat -b localhost:9092 -t admintome-test
cat README | kafkacat -b 192.168.1.240 -t admintome-test
```

### kafka-topics, kafka-console-producer, kafka-console-consumer
```
kubectl exec -ti echoserver-78b58599d6-pthvs bash
kafka-topics.sh --bootstrap-server localhost:9092 --describe
kafka-console-producer.sh --bootstrap-server localhost:9092 --topic streams-plaintext-input
kafka-console-consumer.sh --bootstrap-server localhost:9092 \
    --topic streams-wordcount-output \
    --from-beginning \
    --formatter kafka.tools.DefaultMessageFormatter \
    --property print.key=true \
    --property print.value=true \
    --property key.deserializer=org.apache.kafka.common.serialization.StringDeserializer \
    --property value.deserializer=org.apache.kafka.common.serialization.LongDeserializer

```
