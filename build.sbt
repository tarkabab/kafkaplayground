name := "kafka"

version := "0.1"

scalaVersion := "2.13.2"

libraryDependencies ++= Seq(
  "org.apache.kafka" %% "kafka-streams-scala" % "2.5.0",
  "org.apache.avro"  %  "avro"  %  "1.9.2",
  "org.scalatest"  %%  "scalatest"  %  "3.1.1" % "test"
)
